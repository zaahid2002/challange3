package com.zaahid.chellange3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
    var mylist = arrayListOf(
        DataKata('A', arrayListOf("Abstract","Algorithm","Adapter","Array","Approach")),
        DataKata('B', arrayListOf("Both", "Base","Back-end","Bug","Boolean")),
        DataKata('C', arrayListOf("Code","Compile","Copy","CSS","Char")),
        DataKata('D', arrayListOf("Data","Debug","Class","Description","Default")),
        DataKata('E', arrayListOf("Exceptoion","Error","Example","Engine","Extend")),
        DataKata('F', arrayListOf("Fragment","Format","Final","Field","Front-end","Full-Stack")),
        DataKata('G', arrayListOf("Get","Grid","Gesture","Geometri","Glance")),
        DataKata('H', arrayListOf("Html","Hit","History","Host","Https","Hashcode")),
        DataKata('I', arrayListOf("Input","Instance","Integer","if","Integrity")),
        DataKata('J', arrayListOf("JQuery","Java","Json","JavaScript","Jar","Jaringan")),
        DataKata('K', arrayListOf("Key","Kotlin","kursus","Kind","Keep")),
        DataKata('L', arrayListOf("Library","List","Location","Legacy","Login","Lifecycle")),
        DataKata('M', arrayListOf("Methode","Message","Metadata","Menu","Media","Material"))
    )
    private lateinit var appBarConfiguration : AppBarConfiguration
    private lateinit var navController : NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupNav()
    }
    fun setupNav(){
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration.Builder(navController.graph).build()
        setupActionBarWithNavController(navController,appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() ||  super.onSupportNavigateUp()

    }
}

data class DataKata(
    val huruf : Char,
    val kata : ArrayList<String>
)