package com.zaahid.chellange3.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.zaahid.chellange3.MainActivity
import com.zaahid.chellange3.R
import com.zaahid.chellange3.adapter.FirstAdapter
import com.zaahid.chellange3.databinding.FragmentPertamaBinding

class FragmentPertama : Fragment() {
    private var _binding : FragmentPertamaBinding? = null
    private val binding get()  = _binding!!
    private val setData  = MainActivity().mylist
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPertamaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager =LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        binding.firstRV.isNestedScrollingEnabled = false
        binding.firstRV.layoutManager = layoutManager
        val adapter = FirstAdapter()
        binding.firstRV.adapter = adapter
        adapter.submitData(setData)
    }

}