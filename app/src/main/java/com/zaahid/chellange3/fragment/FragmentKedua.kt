package com.zaahid.chellange3.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.zaahid.chellange3.adapter.SecondAdapter
import com.zaahid.chellange3.databinding.FragmentKeduaBinding


class FragmentKedua : Fragment() {
    private var _binding : FragmentKeduaBinding? = null
    private val binding get()  = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKeduaBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mhuruf = arguments?.getChar("HURUF")
        val mkata = arguments?.getStringArrayList("KEY_KATA")
        val layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        val adapter = SecondAdapter(mkata as ArrayList<String> /* = java.util.ArrayList<kotlin.String> */)
        binding.secondRV.layoutManager = layoutManager
        binding.secondRV.adapter = adapter

    }
}