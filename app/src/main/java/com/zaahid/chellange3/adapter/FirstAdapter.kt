package com.zaahid.chellange3.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zaahid.chellange3.DataKata
import com.zaahid.chellange3.R

class FirstAdapter : RecyclerView.Adapter<FirstAdapter.FirsViewHolder>() {

    private val diffCallback = object: DiffUtil.ItemCallback<DataKata>() {
        override fun areContentsTheSame(oldItem: DataKata, newItem: DataKata): Boolean {
            return oldItem.huruf == newItem.huruf
        }

        override fun areItemsTheSame(oldItem: DataKata, newItem: DataKata): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    val differ =AsyncListDiffer(this,diffCallback)
    override fun getItemCount(): Int = differ.currentList.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirsViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.custom_view,parent,false)
        return FirsViewHolder(view)
    }
    class FirsViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val button = view.findViewById<Button>(R.id.button)

    }

    override fun onBindViewHolder(holder: FirsViewHolder, position: Int) {
        val huruf = differ.currentList[position]

        val mbundle =  Bundle()
        mbundle.putChar("HURUF",huruf.huruf)
        mbundle.putStringArrayList("KEY_KATA",huruf.kata)
        with(holder)
        {
            button.text = huruf.huruf.toString()
            button.setOnClickListener {
                itemView.findNavController().navigate(R.id.action_fragmentPertama_to_fragmentKedua,mbundle)
            }
        }
    }

    fun submitData (value : ArrayList<DataKata>) = differ.submitList(value)
}