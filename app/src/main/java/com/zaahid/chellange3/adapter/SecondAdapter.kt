package com.zaahid.chellange3.adapter

import android.app.SearchManager
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.zaahid.chellange3.R

class SecondAdapter(private val list: ArrayList<String>) :
    RecyclerView.Adapter<SecondAdapter.SecondViewHolder>() {
    override fun getItemCount(): Int = list.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SecondViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_view,parent,false)
        return SecondViewHolder(view)
    }

    override fun onBindViewHolder(holder: SecondViewHolder, position: Int) {
        val mlist = list[position]
        with(holder){
            button.text = mlist
            button.setOnClickListener {
                val intent = Intent(Intent.ACTION_WEB_SEARCH)
                val url = "https://www.google.com/search?q=$mlist"
                intent.putExtra(SearchManager.QUERY,url)
                itemView.context.startActivity(intent)
            }
        }
    }


    class SecondViewHolder(itemView : View):RecyclerView.ViewHolder(itemView){
        val button : Button = itemView.findViewById(R.id.button)
    }
}